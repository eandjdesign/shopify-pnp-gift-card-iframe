/**************************************************
 * Three responses that matter:
 * ------------------------------------------------
 * 1. FinalStatus
 * 2. MErrMsg
 * 3. balance
 * 4. amount
 *
 * ------------------------------------------------
 * Error States to account for:
 * ------------------------------------------------
 * 1. success with gc only
 * 2. insufficient gc funds
 * 3. bad gc (i.e. balance & amount are blank)
 * 4. success with cc + gc
 * 5. bad cc
 *		
 * ------------------------------------------------
 * Interaction Guideline:
 * ------------------------------------------------
 * 1. Submit form to PnP with just GC Fields
 * 2. Parse response
 * 3. If FinalStatus is 'badcard', 'problem' or 'fraud'
 *  	1. write out 'MErrMsg' and 'balance'
 *  	2. display CC Fields
 * 4. send back to PnP
 * 5. parse response
 * 6. if 'FinalStatus' is NOT 'badcard', 'problem' or 'fraud'
 *  	1. Thank you Status
 *
 * ------------------------------------------------
 * Available Test Credit Cards:
 * ------------------------------------------------
 * AmEx:       378282246310005  - 15 digits
 * Visa:       4111111111111111 - 16 digits
 * Mastercard: 5555555555554444 - 16 digits
 * Discover:   6011000990139424 - 16 digits
 *************************************************/

jQuery.noConflict();
jQuery(document).ready(function() {
	var data, key, value, i, pair, finalStatus, balance, balanceForDisplay, errMsg, status, ccInputs, ccInputElement, ccInputNames, msg, monthExpiry, yearExpiry;

	// --- only allow numbers to be entered in the inputs with .numeric
	jQuery(".numeric").numeric(false, function() { alert("Numbers only"); this.value = ""; this.focus(); });

	// --- detect keyup on the credit card number field
  	jQuery(".card-number input").keyup(function() {

		selectCC(jQuery(this).val());
		
		if(jQuery(this).val() == 0) {

			jQuery('.invalid-card').text('');
			jQuery('.icon-credit').removeClass('selected');

		}
	});

	// --- perform some validation as the user leaves the inputs
	jQuery('.card-cvv input').blur(function()  { validateAllTheThings(jQuery(this), 3);  });
	jQuery('.gc-number input').blur(function() { validateAllTheThings(jQuery(this), 19); });
	jQuery('.gc-cvv input').blur(function()    { validateAllTheThings(jQuery(this), 2);  });
	
	
	// --- when user clicks the form submit button, do this stuff
	jQuery('#submitForm').click(function(){
		jQuery('#pnp-info').html('');
	
		bounded();
		
		if(jQuery('.gc-container select').length != 0) {
			validateCcExpiry();

			jQuery('#submitForm').prop("disabled", true);
		} else {
			jQuery('#submitForm').prop("disabled", false);
		}

		// validation success; send to payment gateway
		if(jQuery('.gc-container .msg-warn').length == 0) {
			
			payment_api.send();
			jQuery('.disable-able input, .disable-able select').prop("disabled", true);
			jQuery(this).val('Completing Purchase... Please wait.');

			// remove previous message
			jQuery('#message-contain').slideUp('slow', function(){

				jQuery(this).children('.message').remove();
				jQuery(this).removeAttr('style');

			});
		}

	});
	
	payment_api.setCallback(function(data) {
		jQuery('#submitForm').val('Complete My Purchase');

		// --- Get single key/value pair
		function getVar(variable, data, pairs) {
			for (i = 0; i < pairs.length; i++) {
				pair = pairs[i].split('=');
				if (decodeURIComponent(pair[0]) == variable) {
					return decodeURIComponent(pair[1]);
				}
			}
		}

		// --- set all the useful information we get back to usable variables
		pairs       = data.split('&');
		errMsg      = getVar('MErrMsg',     data, pairs);
		status      = getVar('MStatus',     data, pairs);
		balance     = getVar('balance',     data, pairs);
		amount      = getVar('amount',      data, pairs);
		amountGC    = getVar('amountGC',    data, pairs);
		amountCC    = getVar('amountCC',    data, pairs);
		finalStatus = getVar('FinalStatus', data, pairs);
		cardAmount  = getVar('card_amount', data, pairs);

		// --- for display purposes only
		balanceForDisplay  = balance;
		amountForDisplay   = amount;
		amountGcForDisplay = amountGC;
		amountCcForDisplay = amountCC;

		if(typeof amount != "number")  { amount  = parseFloat(amount);  }
		if(typeof balance != "number") { balance = parseFloat(balance); }

		if(typeof amountGC != "number" && amountGC != undefined) { amountGC = parseFloat(amountGC); }
		if(typeof amountCC != "number" && amountCC != undefined) { amountCC = parseFloat(amountCC); }

		console.log('balance: ' + balance);
		console.log('balance type: ' + typeof balance);
		console.log('amount: ' + amount);
		console.log('amount type: ' + typeof amount);
		console.log('amountGC: ' + amountGC);
		console.log('amountCC: ' + amountCC);
		console.log('cardAmount: ' + cardAmount);
		console.log('----------------------------------------');
		console.log('----------------------------------------');

		/* -----------------------------------------
		if gc only:
			amount == cardAmount
			amountgc & amountcc are undefined
			balance > 0
		if gc + cc:
			balance & amount are undefined
			amountgc & amountcc are not undefined
		if cc only:
			amount is undefined
			balance is undefined
			amountGC is undefined
			amountCC is undefined
		----------------------------------------- */

		if(finalStatus === 'success') {
		
			msg = '<div class="message success">';
			
			// if gc only
			if ( amount == cardAmount && amountGC == undefined && amountCC == undefined ) {
				msg += '<p class="msg-txt">Yay! Thank you for your order!</p>';
				msg += '<p class="msg-txt">$' + amountForDisplay + ' was applied from your gift card or store credit. Your remaining value is $' + balanceForDisplay + '</p>';
			}
			
			// if gc + cc
			else if ( isNaN(amount) && isNaN(balance) && amountGC != undefined && amountCC != undefined ) {
				msg += '<p class="msg-txt">Yay! Thank you for your order!</p>';
				msg += '<p class="msg-txt">$' + amountGcForDisplay + ' was applied from your gift card or store credit. This gift card is now empty.</p>';
				msg += '<p class="msg-txt">$' + amountCcForDisplay + ' was also applied to the credit card you provided.</p>';
			}
			
			// if cc only
			else if ( isNaN(amount) && isNaN(balance) && amountGC == undefined && amountCC == undefined ) {
				msg += '<p class="msg-txt">Yay! Thank you for your order!</p>';
				msg += '<p class="msg-txt">$' + cardAmount + ' was applied to the credit card you provided.</p>';
			}
			
			else {
				msg += '<p class="msg-txt">Yay! Thank you for your order!</p>';
			}
			
			msg += '</div>';

			jQuery('.submit-form').remove();

		}
		else {

			// enable inputs
			jQuery('.disable-able input, .disable-able select').prop("disabled", false);

			msg = '<div class="message error-problem">';

			// if amount or balance are not valid
			if (amount == '' && balance == '') {

				if(errMsg === 'Card number failed luhn10 check') {
				
					// invalid credit card
					msg += '<p class="msg-txt">Sorry! There was a problem with your credit card.</p>';
					msg += '<p class="msg-txt">Please try again.</p></div>';

				} else {

					// invalid gift card
					msg += '<p class="msg-txt">Sorry! There was a problem with your gift card.</p>';
					msg += '<p class="msg-txt">Please <span class="reset-form" onclick="resetForm();">try re-entering your information</span> or call customer service at (917) 460-0404.</p>';

				}
				
			} else {
				
				// provide cc fields since we hit the error state
				if(jQuery('.gc-container .credit-card').length == 0) { addCreditCardFields(); }
				
				// zero balance on gift card
				if(balance == '0.00') {
				
					// moveGiftCardFields();
					msg += '<p class="msg-txt">Oops! Looks like this gift card has a balance of $0.00.</p>';
					msg += '<p class="msg-txt">Please provide your credit card information below to pay for the balance due.</p>';
					msg += '<p class="msg-txt">If you think there is an error with your gift card or store credit, please call customer service at (917) 460-0404.</p>';

				}
				else if (amount > balance) {

					// moveGiftCardFields();
					// insufficient gift card funds
					msg += '<p class="msg-txt">Oops! There is only $' + balanceForDisplay + ' on your gift card or store credit. Your total cost is $' + amountForDisplay + '</p>';
					msg += '<p class="msg-txt">Please provide your credit card information below to pay for the balance due.</p>';
					
				}
				else {
					
					// invalid credit card
					msg += '<p class="msg-txt">Sorry! There was a problem with your credit card.</p>';
					msg += '<p class="msg-txt">Please <span class="reset-form" onclick="resetForm();">try re-entering your information</span> or call customer service at (917) 460-0404.</p>';

				}
				
			}

			msg += '</div>';

		}

		jQuery('#message-contain').html(msg);

		for(i = 0; i < pairs.length; i++) {
			keyValuePair = pairs[i].split('=');
			key          = decodeURIComponent(keyValuePair[0]);
			value        = decodeURIComponent(keyValuePair[1]);

			jQuery('#pnp-info').append('<li><code><strong>' + key + ': </strong>' + value + '</code></li>');
		}

	});

});

// I should shoot myself for doing it this way (via onclick).
function resetForm() {
	jQuery('.gift-card input, .credit-card input').val('');
}


// validation function
function validateAllTheThings(elementToValidate, validationLength) {

	// if element doesn't appear on the page yet, don't validate it.
	if(elementToValidate.length != 0) {
		if(elementToValidate.val().length == validationLength) {
			elementToValidate.parent('.msg-wrapper').removeClass('msg-warn');
			elementToValidate.siblings('.msg-txt').addClass('hide');
		} else if(elementToValidate.val().length == 0) {
			elementToValidate.parent('.msg-wrapper').addClass('msg-warn');
			elementToValidate.siblings('.msg-txt').removeClass('hide');
			elementToValidate.siblings('.msg-txt').text('Cannot be empty');
		} else {
			elementToValidate.parent('.msg-wrapper').addClass('msg-warn');
			elementToValidate.siblings('.msg-txt').removeClass('hide');
			elementToValidate.siblings('.msg-txt').text('Please enter ' + validationLength + ' numbers.');
		}
	}	
}


// validate the expiry dropdowns. make sure there's a valid month/year combo
function validateCcExpiry(){

	monthExpiry = jQuery('#month_exp :selected');
	yearExpiry  = jQuery('#year_exp :selected');

	if(monthExpiry.val() == '00' || yearExpiry.val() == '00') {
		jQuery(monthExpiry).parent('select').parent('.msg-wrapper').addClass('msg-warn');
		jQuery(monthExpiry).parent('select').siblings('.msg-txt').removeClass('hide');
	} else {
		jQuery(monthExpiry).parent('select').parent('.msg-wrapper').removeClass('msg-warn');
		jQuery(monthExpiry).parent('select').siblings('.msg-txt').addClass('hide');
		jQuery('#submitForm').prop("disabled", false);
	}
}


// function that adds the credit card fields to the 
// form as well as updating the name attributes to ids
function addCreditCardFields() {
	
	// --- collect credit card inputs
	ccInputs = jQuery('.credit-card input, .credit-card select');
	
	ccInputsDomElement = jQuery('.credit-card');
	
	jQuery.each(ccInputs, function(i) {
	
		ccInputElement = jQuery(ccInputs[i]);
		ccInputNames   = ccInputElement.attr('name');
		ccInputElement.removeAttr('name');
	
		ccInputElement.attr('id', ccInputNames);
	
	});
	
	// Insufficient funds means I need your CC info
	jQuery('#message-contain').after(ccInputsDomElement);
	ccInputsDomElement.removeClass('hide');
}


// function that moves the gift card fields to the
// form as well as updating the name attributes to ids
function moveGiftCardFields () {
	var container = jQuery('.gift-card');
	
	jQuery('form').before(container);
	jQuery('.gc-container .gift-card').remove();

}


// function to select and display the correct credit card image
function selectCC(ccnum) {

	var first  = ccnum.charAt(0),
		second = ccnum.charAt(1),
		third  = ccnum.charAt(2),
		fourth = ccnum.charAt(3);
	
	jQuery('.invalid-card').text('');
	jQuery('.icon-credit').removeClass('selected');
	
	jQuery('.card-number input').blur(function() {
		if(jQuery('.card-number input').val().length == 0) {
			validateAllTheThings(jQuery('.card-number input'), 16);
		}
	});
	if (first == "4") {

		// Visa
		jQuery('.icon-visa').addClass('selected');
		jQuery('.icon-credit').fadeIn('fast');
		jQuery('.card-number input').blur(function() {
			validateAllTheThings(jQuery('.card-number input'), 16);
		});
	
	}
	else if ( (first == "3") && ((second == "4") || (second == "7")) ) {

		// American Express
		jQuery('.icon-amex').addClass('selected');
		jQuery('.icon-credit').fadeIn('fast');
		jQuery('.card-number input').blur(function() {
			validateAllTheThings(jQuery('.card-number input'), 15);		
		});
	
	}
	else if ( (first == "5") ) {

		// Mastercard
		jQuery('.icon-mastercard').addClass('selected');
		jQuery('.icon-credit').fadeIn('fast');
		jQuery('.card-number input').blur(function() {
			validateAllTheThings(jQuery('.card-number input'), 16);
		});
	
	}
	else if ( (first == "6") && (second == "0") && (third == "1") && (fourth == "1") ) {

		// Discover
		jQuery('.icon-discover').addClass('selected');
		jQuery('.icon-credit').fadeIn('fast');
		jQuery('.card-number input').blur(function() {
			validateAllTheThings(jQuery('.card-number input'), 16);
		});
	
	}
	else {
	
		// --- don't alert user until the 4th character has been input
		if((second != '') && (third != '') && (fourth != '')) {

			jQuery('.icon-credit').fadeOut();
			jQuery('.invalid-card').text('Invalid Credit Card Number, please retry.');
		}

	}
}

function bounded(){
	jQuery('select').bind('change', function(){
		validateCcExpiry()
	});
}
