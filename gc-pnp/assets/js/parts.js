var CCInfo, msgWarn, msgErr, msgSuccess, finalStatus, status, balance;

/* --- Credit Card Info Partial --- */
CCInfo  =		'<div class="info-section credit-card-info">';
CCInfo +=			'<h2>Credit Card Information</h2>';
CCInfo +=			'<ul>';
CCInfo +=				'<li class="disable-able"><label for="card_name">your name</label><input type="text" id="card_name" value="cardtest"></li>';
CCInfo +=				'<li class="disable-able"><label for="card_number">card number</label><input type="text" id="card_number" value="4111111111111111"></li>';
CCInfo +=				'<li class="disable-able"><label for="card_exp">expiry date</label><input type="text" id="card_exp" value="01/15"> [MM/YY]</li>';
CCInfo +=				'<li class="disable-able"><label for="card_cvv">card cvv</label><input type="text" id="card_cvv" value="123"></li>';
CCInfo +=			'</ul>';
CCInfo +=		'</div>';

/* --- Warning Message Partial --- */
msgWarn  =		'<div class="status msg msg-warn">';
msgWarn +=			'<p>';
msgWarn +=				'<strong>Hmm..</strong><br />';
msgWarn +=				'Your gift card balance is: <strong>$1.00</strong> and is insufficient for this purchase.';
msgWarn +=			'</p>';
msgWarn +=			'<p>Please provide credit card information in order to complete purchase.</p>';
msgWarn +=		'</div>';

/* --- Error Message Partial --- */
msgErr  =		'<div class="status msg msg-err">';
msgErr +=			'<p>';
msgErr +=				'<strong>Oops!</strong><br />';
msgErr +=				'There was an error in processing your credit card.';
msgErr +=			'</p>';
msgErr +=			'<p>Please check that you\'ve entered the information correctly, and try again. If you continue to receive errors, please try another gift card or credit card</p>';
msgErr +=		'</div>';

/* --- Success Message Partial --- */
msgSuccess  =	'<div class="status msg msg-success">';
msgSuccess +=		'<p>';
msgSuccess +=			'<strong>Yay!</strong><br />';
msgSuccess +=			'Thank you for your order! The balance on your card is now: <strong>$1.00</strong>.';
msgSuccess +=		'</p>';
msgSuccess +=		'<p>You\'ll now be redirected to our <a href="http://oneluckyduck.com" title="Return to One Lucky Duck">homepage</a>.</p>';
msgSuccess +=	'</div>';

