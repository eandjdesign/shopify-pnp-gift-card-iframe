<!doctype html>

<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title>One Lucky Duck/PNP iFrame Test Page</title>
		<link rel="stylesheet" href="/gc-pnp/assets/css/style.css" type="text/css">

	</head>
	<body>
		<div class="gc-container">
			<h2>Instructions for Gift Card and Store Credit</h2>
			<form method="post" class="form">

				<!-- hidden form fields -->
				<input type="hidden" id="publisher_name" value="oneluckydu">
				<input type="hidden" id="publisher_email" value="info@oneluckyduck.com">
				<input type="hidden" id="mode" value="auth">
				<input type="hidden" id="convert" value="underscores">

				<ul class="cost form">
					<li class="cost">
						<label for="card_amount">Total Cost $:</label>
						<input id="card_amount" type="text" value="7.00">
					</li>
				</ul>

				<ul class="gift-card form">
					<li class="gc-number disable-able">
						<label for="mpgiftcard">Gift Card or Store Credit #:</label>
						<span class="msg-wrapper">
							<p class="msg-txt hide"></p>
							<input id="mpgiftcard" class="numeric" type="text" size="23" maxlength="19" value="6050110000007706722" placeholder="First 19 Digits">
						</span>
						<span class="helper-img"></span>
					</li>
					<li class="gc-cvv disable-able">
						<label for="mpcvv">Security Code</label>
						<span class="msg-wrapper">
							<p class="msg-txt hide"></p>
							<input id="mpcvv" class="numeric" type="text" size="2" maxlength="2" value="14" placeholder="xx">
						</span>
						<span class="helper-img"></span>
					</li>
				</ul>

				<div id="message-contain"></div>

				<ul class="submit-form disable-able">
					<li><input id="submitForm" type="submit" value="Complete My Purchase"></li>
				</ul>

			</form>

		</div>				
		<ul class="credit-card form hide">
			<li class="card-name">
				<label for="card_name">Name on Card</label>
				<span class="msg-wrapper">
					<p class="msg-txt hide">Please provide your name</p>
					<input type="text" name="card_name" value="cardtest" maxlength="16" placeholder="Enter your name">
				</span>
			</li>
			<li class="card-number disable-able">
				<label for="card_number">Credit Card #</label>
				<span class="msg-wrapper">
					<p class="msg-txt hide"></p>
					<input type="text" class="numeric" name="card_number" value="4111111111111111" maxlength="16" placeholder="Enter Credit Card">
				</span>
				<span class="credit-card-img">
					<i class="icon-credit icon-visa"></i>
					<i class="icon-credit icon-mastercard"></i>
					<i class="icon-credit icon-amex"></i>
					<i class="icon-credit icon-discover"></i>
					<span class="invalid-card"></span>
				</span>
			</li>
			<li class="card-cvv disable-able">
				<label for="card_cvv">Security Code</label>
				<span class="msg-wrapper">
					<p class="msg-txt hide"></p>
					<input type="text" class="numeric" name="card_cvv" value="123" size="2" maxlength="3" placeholder="xxx">
				</span>
			</li>
			<li class="card-exp disable-able">
				<label for="cc-expiry-month">Expiration</label>
				<span class="msg-wrapper msg-warn">
					<p class="msg-txt hide">Please select a month and year</p>
					<select name="month_exp">
						<option value="00" selected>Month</option>
						<option value="01">01 - January</option>
						<option value="02">02 - February</option>
						<option value="03">03 - March</option>
						<option value="04">04 - April</option>
						<option value="05">05 - May</option>
						<option value="06">06 - June</option>
						<option value="07">07 - July</option>
						<option value="08">08 - August</option>
						<option value="09">09 - September</option>
						<option value="10">10 - October</option>
						<option value="11">11 - November</option>
						<option value="12">12 - December</option>
					</select>
					<select name="year_exp">
						<option value="00" selected>Year</option>
						<option value="13">2013</option>
						<option value="14">2014</option>
						<option value="15">2015</option>
						<option value="16">2016</option>
						<option value="17">2017</option>
						<option value="18">2018</option>
						<option value="19">2019</option>
						<option value="20">2020</option>
					</select>
				</span>
			</li>
		</ul>


		<ul id="pnp-info"></ul>

		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
		<script>window.jQuery || document.write('<script src="/gc-pnp/assets/js/jquery-1.9.1.min.js"><\/script>')</script>

 		<script src="https://pay1.plugnpay.com/api/iframe/CrrqZOtglb/client/nonconflict/"></script>
		<script src="/gc-pnp/assets/js/jquery.numeric.js"></script>
		<script src="/gc-pnp/assets/js/script.js"></script>

	</body>

</html>